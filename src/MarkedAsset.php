<?php

namespace exoo\marked;

use yii\web\AssetBundle;

class MarkedAsset extends AssetBundle
{
    public $sourcePath = '@npm/marked';

    public $js = [
        'marked.min.js',
    ];
}
